#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS 1

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 1
#define TRIG_PIN 0
#define ECHO_PIN 2

// Define the array of leds
CRGB leds[NUM_LEDS];



// Anything over 400 cm (23200 us pulse) is "out of range"
const unsigned int MAX_DIST = 23200;

const int numReadings = 10;

unsigned long pulse_width[numReadings];
int readIndex = 0;              // the index of the current reading
int total = 0;  
float cm = 0;

void setup() {

  // The Trigger pin will tell the sensor to range find
  pinMode(TRIG_PIN, OUTPUT);
  digitalWrite(TRIG_PIN, LOW);
  //  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

  for (int i = 0; i < numReadings; i++) {
    pulse_width[i] = 0;
  }
}

void loop() {
  // subtract the last reading:
  total = total - pulse_width[readIndex];
  // read from the sensor:

  // Hold the trigger pin high for at least 10 us
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  pulse_width[readIndex] = pulseIn(ECHO_PIN, HIGH);



    // add the reading to the total:
  total = total + pulse_width[readIndex];

  cm = (total / numReadings) / 58.0;
  
  if ( pulse_width[readIndex] > MAX_DIST ) {
    leds[0] = CRGB::Black;
  } else {
    leds[0] = CHSV(map(cm, 0, 400, 0, 255), 255, 255);
  }

    // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }
  
  FastLED.show();
  delay(100);
}
