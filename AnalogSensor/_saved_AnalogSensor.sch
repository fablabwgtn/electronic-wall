EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Custom Lib
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x01 J1
U 1 1 5D1ECF7A
P 5250 4500
F 0 "J1" H 5250 4600 50  0000 C CNN
F 1 "Conn_01x01" H 5250 4400 50  0000 C CNN
F 2 "Custom_Lib:Conn_Sewing-Hole-Oval-Large" H 5250 4500 50  0001 C CNN
F 3 "" H 5250 4500 50  0001 C CNN
	1    5250 4500
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J3
U 1 1 5D1ECFD4
P 5750 4500
F 0 "J3" H 5750 4600 50  0000 C CNN
F 1 "Conn_01x01" H 5750 4400 50  0000 C CNN
F 2 "Custom_Lib:Conn_Sewing-Hole-Oval-Large" H 5750 4500 50  0001 C CNN
F 3 "" H 5750 4500 50  0001 C CNN
	1    5750 4500
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J4
U 1 1 5D1ECFF6
P 6200 4500
F 0 "J4" H 6200 4600 50  0000 C CNN
F 1 "Conn_01x01" H 6200 4400 50  0000 C CNN
F 2 "Custom_Lib:Conn_Sewing-Hole-Oval-Large" H 6200 4500 50  0001 C CNN
F 3 "" H 6200 4500 50  0001 C CNN
	1    6200 4500
	1    0    0    -1  
$EndComp
$Comp
L Audio-Jack-3 J2
U 1 1 5D1ED0CF
P 5600 4100
F 0 "J2" H 5550 4275 50  0000 C CNN
F 1 "Audio-Jack-3" H 5700 4030 50  0000 C CNN
F 2 "Custom_Lib:2.5MMJACK" H 5850 4200 50  0001 C CNN
F 3 "" H 5850 4200 50  0001 C CNN
	1    5600 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4000 6000 4000
Wire Wire Line
	6000 4000 6000 4500
Wire Wire Line
	5800 4100 5850 4100
Wire Wire Line
	5850 4100 5850 4350
Wire Wire Line
	5850 4350 5550 4350
Wire Wire Line
	5550 4350 5550 4500
Wire Wire Line
	5400 4200 5400 4300
Wire Wire Line
	5400 4300 5050 4300
Wire Wire Line
	5050 4300 5050 4500
$Comp
L LED D1
U 1 1 5D225DEE
P 6000 3500
F 0 "D1" H 6000 3600 50  0000 C CNN
F 1 "LED" H 6000 3400 50  0000 C CNN
F 2 "LEDs:LED_1206_HandSoldering" H 6000 3500 50  0001 C CNN
F 3 "" H 6000 3500 50  0001 C CNN
	1    6000 3500
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5D225E1F
P 6450 3500
F 0 "R1" V 6530 3500 50  0000 C CNN
F 1 "R" V 6450 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6380 3500 50  0001 C CNN
F 3 "" H 6450 3500 50  0001 C CNN
	1    6450 3500
	0    1    1    0   
$EndComp
$Comp
L Conn_01x01 J6
U 1 1 5D225E63
P 6900 3500
F 0 "J6" H 6900 3600 50  0000 C CNN
F 1 "Conn_01x01" H 6900 3400 50  0000 C CNN
F 2 "Custom_Lib:Conn_Sewing-Hole-Oval-Large" H 6900 3500 50  0001 C CNN
F 3 "" H 6900 3500 50  0001 C CNN
	1    6900 3500
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J5
U 1 1 5D225EAD
P 5500 3500
F 0 "J5" H 5500 3600 50  0000 C CNN
F 1 "Conn_01x01" H 5500 3400 50  0000 C CNN
F 2 "Custom_Lib:Conn_Sewing-Hole-Oval-Large" H 5500 3500 50  0001 C CNN
F 3 "" H 5500 3500 50  0001 C CNN
	1    5500 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 3500 6600 3500
Wire Wire Line
	6300 3500 6150 3500
Wire Wire Line
	5850 3500 5700 3500
$EndSCHEMATC
