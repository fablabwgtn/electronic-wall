/* LED Strip Multiple Modes Demo
 *  
 *  Oringal functions: Daniel Harmsworth, 2019
 *  Compliation: Harry, 2019
 *  Fab Lab Wgtn
 */
#include <NeoPixelBus.h>

// How many colours do you want in your chase pattern?
#define COLOURS 3

// How many LED's per colour?
#define BLOCK_SIZE 12

// How long in milliseconds between each 'step' in the chase pattern?
#define STEP_DELAY 1

//Define your colours here in R, G and B. You need to have the same number of colours here as was defined above
RgbColor Colours[COLOURS] = {   RgbColor(5,150,143),    // You need a comma after each entry
                                RgbColor(248,151,29), 
                                RgbColor(237,23,80)     // Except for the last one, there's nothing after it so it doesn't need a comma.
                            };

// How many LEDs on the strip?
const uint16_t PixelCount = 300; // There are 285 LEDs in the hallway strip.

// What pin is the LED strip attached to?
const uint8_t PixelPin = 2;

NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(PixelCount, PixelPin);

int variable = 0;

void setup()
{
    strip.Begin();
    strip.Show();
}


void loop()
{
    fabSweep(BLOCK_SIZE,STEP_DELAY);
}

void fabSweep(int blockSize, int stepDelay) {
 
  RgbColor blocks[blockSize * COLOURS];
  
  for (int i = 0; i < COLOURS; i++){
    for (int j = 0; j < blockSize; j++) {
      blocks[(i*blockSize)+j] = Colours[i];
    }
  }

  int blockPos;
  for (int i = 0; i < blockSize*COLOURS; i++) {
    blockPos = i;
    for ( int pixel = 0; pixel < PixelCount; pixel++ ) {
      if (blockPos >= blockSize * COLOURS) { blockPos = 0; }
      strip.SetPixelColor(pixel, blocks[blockPos]);
      blockPos++;
    }
    strip.Show();
    delay(stepDelay);
  }
