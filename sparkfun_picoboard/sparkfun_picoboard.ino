// DigitalSandboxPico
//
// PicoBoard Firmware modified for the DigitalSandBox 
// Arduino Learning Platform.
// Ported over from original C code.
//
// Modified by: Brian Huang, Sparkfun Electronics
// Date:  August 7, 2014

// UPLOAD WITH ARDUINO USING LILYPAD W/ ATMEGA328


char request = 0;
unsigned int sensor_value = 0;
char data_packet[2]= "";

int SLIDER = A3; // Slider on DS Board
int SOUND = A2;  // Microphone on DS Board
int LIGHT = A1;  // Light Detector on DS Board
int BUTTON = 12; // Push Button on DS Board

int RA = A0; // Temp
int RB = 2;  // Switch
int RC = 3;  // D3 -- Side Port 
int RD = A4; // A4 -- Top Port

void setup()
{
  pinMode(BUTTON, INPUT_PULLUP);  // enable internal pull-up resistor
  pinMode(RB, INPUT_PULLUP);  // enable internal pull-up resistor
  Serial.begin(38400);
}

void loop(){
  
  //Read/Report channel 0 (Resistance-D)
  sensor_value=analogRead(RD);
  
  
  //Read/Report Channel 1 (Resistance-C)
  sensor_value=analogRead(RC);
  
  //Read/Report Channel 2 (Resistance-B)
  sensor_value=1023*digitalRead(RB);
   
  
  //Read/Report Channel 3 (Button)
  sensor_value = 1023*(1 - digitalRead(BUTTON));
  
  //Read/Report Channel 4(Resistance-A)
  sensor_value=analogRead(RA);
  Serial.println(sensor_value);
  
  //Read/Report Channel 5(LIGHT)
  sensor_value=1023 - analogRead(LIGHT);
  
  
  //Read/Report Channel 6(Sound)
  sensor_value=analogRead(SOUND);
    
  
  //Read/Report Channel 7(Slider)
  sensor_value=analogRead(SLIDER);

  delay(100);
}
