/* Multi Mode Demo
 *
 *  Based on FastLED DEMOREEL100 Example
 *  Modified by Harry Iliffe, 2019
 *  Fab Lab WGTN
 *
 */

#include <FastLED.h>
#include <MPR121.h>
#include <Wire.h>

#define DATA_PIN_STRIP1    7
#define DATA_PIN_STRIP2    2
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    300
CRGB leds[NUM_LEDS];

int brightness = 96;
#define FRAMES_PER_SECOND  120

#define MPR121_ADDR 0x5C
#define MPR121_INT 4


#define BUTTON_BRIGHTNESSDOWN 0
#define BUTTON_BRIGHTNESSUP 1
#define BUTTON_SPEEDDOWN 2
#define BUTTON_SPEEDUP 3
#define BUTTON_MODEDOWN 4
#define BUTTON_MODEUP 5

void setup() {
  Serial.begin(57600);

  delay(3000); // 3 second delay for recovery

  if(!MPR121.begin(MPR121_ADDR)) Serial.println("error setting up MPR121");
  MPR121.setInterruptPin(MPR121_INT);

  MPR121.setTouchThreshold(40);
  MPR121.setReleaseThreshold(20);

  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN_STRIP1,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
//  FastLED.addLeds<LED_TYPE,DATA_PIN_STRIP2,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);


  // set master brightness control
  FastLED.setBrightness(brightness);
}

// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm, colourChase };
uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns
int speed = 1;
void loop()
{
  // Call the current pattern function once, updating the 'leds' array
  gPatterns[gCurrentPatternNumber]();

  // send the 'leds' array out to the actual LED strip
  FastLED.show();
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND);

  // do some periodic updates
  gHue += speed;
//  EVERY_N_SECONDS( 10 ) { nextPattern(true); } // change patterns periodically

  readTouchInputs();
}

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))


void nextPattern(bool forward)
{
  if(forward){
    // add one to the current pattern number, and wrap around at the end
    gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
  } else {
    // subtract one from the current pattern number, and wrap around at the end
    gCurrentPatternNumber = (gCurrentPatternNumber - 1)==-1 ? ARRAY_SIZE( gPatterns)-1 : (gCurrentPatternNumber - 1) ;
  }
  Serial.println("Mode: " + String(gCurrentPatternNumber));
}

void readTouchInputs(){
  if(MPR121.touchStatusChanged()){

    MPR121.updateTouchData();

    // only make an action if we have one or fewer pins touched
    // ignore multiple touches

    if(MPR121.getNumTouches()<=1){
      for (int i=0; i < 12; i++){  // Check which electrodes were pressed
        if(MPR121.isNewTouch(i)){
          digitalWrite(LED_BUILTIN, HIGH);
          switch(i){
            case BUTTON_BRIGHTNESSDOWN:
              brightness = constrain(brightness-16, 0, 255);
              FastLED.setBrightness(brightness);
              Serial.println("Brightness: " + String(brightness));
              break;
            case BUTTON_BRIGHTNESSUP:
              brightness = constrain(brightness+16, 0, 255);
              FastLED.setBrightness(brightness);
              Serial.println("Brightness: " + String(brightness));
              break;
            case BUTTON_MODEDOWN:
              nextPattern(false);
              break;
            case BUTTON_MODEUP:
              nextPattern(true);
              break;
            case BUTTON_SPEEDDOWN:
              speed = constrain(speed--, 0, 100);
              Serial.println("Speed: " + String(speed));
              break;
            case BUTTON_SPEEDUP:
              speed = constrain(speed++, 0, 100);
              Serial.println("Speed: " + String(speed));
              break;
          }
        }else{
          if(MPR121.isNewRelease(i)){
           Serial.print("pin ");
           Serial.print(i);
           Serial.println(" is no longer being touched");
            digitalWrite(LED_BUILTIN, LOW);
         }
        }
      }
    }
  }
}




void rainbow()
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter()
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter)
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti()
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 3*speed, 0, NUM_LEDS-1 );
  leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( 15*speed, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    leds[beatsin16( i+7, 0, NUM_LEDS-1 )] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

void colourChase(){

  CRGB colour[] = {  CRGB(5,150,143),
                    CRGB(248,151,29),
                    CRGB(237,23,80),
                    CRGB(0,0,0)
  };

  CRGBPalette16 palette = CRGBPalette16(
                                  colour[0], colour[0],
                                  colour[1], colour[1],
                                  colour[2], colour[2],
                                  colour[3], colour[3],
                                  colour[0], colour[0],
                                  colour[1], colour[1],
                                  colour[2], colour[2],
                                  colour[3], colour[3]
                                  );

    for( int i = 0; i < NUM_LEDS; i++) {
      leds[i] = ColorFromPalette( palette, gHue+(i*2), 255);
    }
}
