#define NUM_SENSORS 5

struct sensor {
  int pin;
  int led;
  int lower;
  int higher;
  int value;
} sensor[] = {
  {A0, 5, 40, 1023}, //Button
  {A1, 6, 40, 1023}, //Stroke Sensor
  {A2, 9, 40, 800}, //Tilt Switch
  {A3, 10, 40, 1023}, //
  {A4, 11, 40, 1023},
  {A5, 3, 40, 1023}
};


void setup() {
  for(int i=0;i<NUM_SENSORS;i++){
    pinMode(sensor[i].pin, INPUT_PULLUP);
    pinMode(sensor[i].led, OUTPUT);
  }
  Serial.begin(9600);
}

void loop() {
  for(int i=0;i<NUM_SENSORS;i++){
    
    sensor[i].value = map(analogRead(sensor[i].pin),1023,0,sensor[i].lower,sensor[i].higher);
    Serial.print(String(i) + ": " + String(sensor[i].value) + "\t");
    analogWrite(sensor[i].led, map(sensor[i].value, sensor[i].lower,sensor[i].higher, 0, 255));
  }
  Serial.println();
  delay(100);
}
